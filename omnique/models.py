from django.db import models

# product model
class Product(models.Model):
    name = models.CharField(max_length=64)
    description = models.TextField(max_length=512)
    registered_designs = models.ManyToManyField('Design')
    registered_tags = models.ManyToManyField('Tag')
    price = models.DecimalField(max_digits=999,decimal_places=2)
    has_price_range = models.BooleanField(default=False)
    category_options = {('AD','Adults\' Clothing'),('CH','Childrens\' Clothing'),('BB','Baby Clothes'),('OT','Other Products')}
    category = models.CharField(max_length=2,choices=category_options,default="OT")
    img = models.FileField(upload_to='images/products/')


    class Meta:
        verbose_name = 'Product'
        verbose_name_plural = 'Products'
        ordering = ['name']

    def __str__(self):
        return self.name

# design model
class Design(models.Model):
    name = models.CharField(max_length=64)
    img = models.FileField(upload_to='images/designs/')

    class Meta:
        verbose_name = 'Design'
        verbose_name_plural = 'Designs'
        ordering = ['name']

    def __str__(self):
        return self.name

# tag model
class Tag(models.Model):
    name = models.CharField(max_length=64, blank=True)

    class Meta:
        verbose_name = 'Tag'
        verbose_name_plural = 'Tags'
        ordering = ['name']

    def __str__(self):
        return self.name
