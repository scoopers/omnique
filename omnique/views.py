from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound
from django.urls import reverse
from .forms import *
from django.core.mail import EmailMessage
import csv
import os
from .models import Product

def about(request):
    return render(request, "omnique/about.html")

def price_list(request):
    #temp views while e-commerce being implemented
    working_path = os.path.dirname(os.path.abspath(__file__))
    file_path = os.path.join(working_path, 'static/omnique/files/priceList.csv')
    with open(file_path, 'r') as file:
        reader = csv.reader(file)
        price_list = list(reader)

    return render(request, "omnique/shop.html", {'price_list': price_list})

def contact(request):
    resp = False
    if request.method == 'POST':
        # create a form instance and populate it with data from the request
        form = ContactForm(request.POST)
        # check whether it's valid
        if form.is_valid():
            # get post data
            name = form.cleaned_data['name']
            email = form.cleaned_data['email']
            re_email = form.cleaned_data['confirm_email']
            message = form.cleaned_data['message']

            if re_email != email:
                vmail = True
                return render(request, "omnique/contact.html", {'form' : form, 'vmail' : vmail})

            message_to_email = "From: %s at %s \nMessage:\n%s" % (name, email, message)

            subject_to_email = "%s\'s Enquiry" % (name)

            try:
                email = EmailMessage(subject_to_email, message_to_email, to=['example@email.com'])
                email.send(fail_silently=False)
                # send to new url
                return HttpResponseRedirect(reverse('contact'))
            except:
                resp=True
                form = ContactForm()

    # if a GET (or any other method) we'll create a blank form
    else:
        form = ContactForm()
    return render(request, "omnique/contact.html", {'form' : form, 'resp' : resp})

def browse_items(request):
    categories = Product.category_options
    if request.method == 'POST':
        checked_boxes = request.POST.getlist('checks')
        if(len(checked_boxes)==0):
            products = Product.objects.all()
        else:
            products = Product.objects.filter(category__in=checked_boxes)
    else:
        products = Product.objects.all()
    return render(request, "omnique/browseItems.html", {'products':products, 'categories':categories})

def product_details(request, id):
    try:
        product = Product.objects.get(pk=id)
    except:
        return HttpResponseNotFound("No such product!")
    return render(request, "omnique/productDetails.html", {'product': product})
