from django.urls import path
from . import views

urlpatterns = [
    # home page

    path('', views.about, name='about'),
    path('price-list/', views.price_list, name='price_list'),
    path('contact-us/', views.contact, name='contact'),
    path('browse-items/', views.browse_items, name='browse_items'),
    path('prodcuts/<int:id>/', views.product_details, name='product_details')
    ##path('member-<int:member_id>/', views.member_page, name='member'),
]
