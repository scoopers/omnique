from django.apps import AppConfig


class OmniqueConfig(AppConfig):
    name = 'omnique'
